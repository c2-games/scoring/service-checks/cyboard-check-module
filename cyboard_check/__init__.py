from typing import Tuple, Callable

from .results import *
from .variables import *
from .args import *
from .c2games_check import C2GamesCheck
