import argparse
import cyboard_check.generate_vars

parser = argparse.ArgumentParser(prog="cyboard_check")
subparsers = parser.add_subparsers(help='Command to Execute', dest='cmd', required=True)

vars_cmd = subparsers.add_parser('vars', help='Generate a Vars File')
vars_cmd.add_argument('-c', '--check', help="Alias for Check Variables File")
vars_cmd.add_argument('-t', '--team', help="Alias for Team Variables File")
vars_cmd.add_argument('-e', '--event', help="Alias for Event Variables File")
vars_cmd.add_argument(
    '-i', '--input-file', nargs='*', default=[], dest="inputs",
    help="Variable Input file. Filepath must be prefixed with variable type. Ex, check:vars/ftp.json"
)

args = parser.parse_args()

if args.cmd == 'vars':
    cyboard_check.generate_vars.main(args)
