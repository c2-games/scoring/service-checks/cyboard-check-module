import argparse


def get_arg_parser(*args, parser=None, **kwargs) -> argparse.ArgumentParser:
    """
    Get a new Argument Parser object with preconfigured arguments for standard C2Games options.

    Any args passed will be passed to argparse.ArgumentParser.
    """
    if not parser:
        parser = argparse.ArgumentParser(*args, **kwargs)

    # Variables file to load
    parser.add_argument('--vars', metavar='vars.json', help='variables file', required=True)
    # Target Host, used as an override when testing
    parser.add_argument('--host', metavar='HOST', help='Target Host Override')
    # Print debug information
    parser.add_argument('-v', '--verbose', help='print debug information', action='store_true')
    # Name of the check being executed, used to determine the target host
    parser.add_argument('-C', '--check-name', help='name of check being executed')
    # Competition ID of the team being checked, commonly used by checks to determine the team's configuration.
    parser.add_argument('-I', '--competition-id', help='Competition ID of the team being checked.')
    # Shared Timeout Argument. Not all checks support this, but a large number do.
    parser.add_argument(
        '-t', '--timeout', default=1, type=int, help='Timeout for command (not supported by all checks)'
    )

    return parser
