from __future__ import annotations

import random
import string
import uuid
from os import PathLike
from typing import List, Optional, Union, Dict, Callable
import datetime
from pathlib import Path
from uuid import UUID

import yaml
from pydantic import BaseModel, Extra, Field
import jinja2


# Code for "_now_datetime" sourced from:
# https://github.com/ansible/ansible/blob/6d0aeac1e166842f2833f4fb64c727cc7f818118/lib/ansible/template/__init__.py#L801
# Code licensed under GNU General Public License version 3
# See http://www.gnu.org/licenses/ for full license text
def _now_datetime(utc=True, fmt=None):
    """jinja2 global function to return current datetime, potentially formatted via strftime"""
    if utc:
        now = datetime.datetime.utcnow()
    else:
        now = datetime.datetime.now()

    if fmt:
        return now.strftime(fmt)

    return now


def _random_string(length: int = 10, chars: str = None):
    """Generate a random string with given length and character set."""
    if chars is None:
        chars = string.ascii_letters + string.digits
    return ''.join(random.choice(chars) for _ in range(length))


def default_jinja_globals() -> Dict[str, Callable]:
    """Return default jinja global functions"""
    return {
        'now': _now_datetime,
        'uuid': uuid.uuid4,
        'random_string': _random_string,
    }


class GenericVariables(BaseModel, extra=Extra.allow):
    """Empty base class to parse out anything"""


class DeploymentVariables(GenericVariables):
    """Deployment specific variables"""
    host: str


class CheckHostTemplate(GenericVariables):
    """Subclass for host ip templating for each check type"""
    host: str
    name: str
    subnet: str


class EventVariables(GenericVariables):
    """Event specific variables"""
    checks: List[CheckHostTemplate]
    subnets: Dict[str, str]


class TeamMetadata(GenericVariables):
    """Team specific metadata"""
    id: UUID
    name: str
    long_name: Optional[str]
    competition_id: Optional[int]


class InputVariables(BaseModel):
    """Base variables set for checks"""
    # Classes representing the different check variable files
    # Variables that are the same for all teams and unique to each check.
    check: Optional[GenericVariables] = None
    #: Variables that are unique to each team and check.
    deployment: Optional[DeploymentVariables] = None
    #: Variables that are the same for all teams and checks.
    event: Optional[EventVariables] = None
    #: Variables that are unique to each team and the same across all checks.
    team: Optional[GenericVariables] = None
    #: Team Metadata like id, name, etc.
    team_metadata: Optional[TeamMetadata]
    # Name of check being executed, ex, www_content or internal_forward_dns
    check_name: Optional[str]
    jinja_globals: Dict[str, Callable] = Field(default_factory=default_jinja_globals)

    def deployment_host_from_vars(self) -> str:
        """Extract the deployment host if provided"""
        return self.deployment.host if self.deployment and self.deployment.host else ""

    def target_ip_from_vars(self, check_name: Optional[str] = None) -> str:
        """Get the most probably target IP from variables"""
        # try to get the host from deployment vars
        ret = self.deployment_host_from_vars()
        check_name = check_name or self.check_name

        if ret:
            # If we found the Target from deployment vars, this is the most specific, use that
            return ret

        if not (self.event and self.event.checks and check_name):
            # If we can't find Target from event.checks, we don't know what else to do
            raise RuntimeError(
                'Target IP not found in deployment vars and'
                ' not enough information exists to search the event configuration.'
            )

        # Search event.checks for check_name, and derive deployment info from that
        for e in self.event.checks:
            if e.name != check_name:
                continue
            net = self.event.subnets[e.subnet]
            net = self.jinja_render_vars(net)
            check_host = self.jinja_render_vars(e.host)
            ret = ip_template(check_host, net)

        if not ret:
            # Host still not found. Raise an exception to avoid false positives
            raise RuntimeError('Target IP was not found')

        return ret

    def jinja_render_vars(self, template: str) -> str:
        """Apply Jinja Templating to `template` using the variables specified by `v`."""
        tmpl = jinja2.Template(template)
        for name, func in self.jinja_globals.items():
            tmpl.globals[name] = func
        return tmpl.render(vars=self, **self.dict())

    @classmethod
    def variables_from_file(cls, filename: Union[str, bytes, PathLike, Path], **kwargs) -> InputVariables:
        """Load variables from a json file"""
        content = {}
        with open(filename) as f:
            content = yaml.safe_load(f)
        return cls(**content, **kwargs)


def _replace_x(template: List[str], parts: List[str]) -> List[str]:
    """Replace instances of X at the same index from parts"""
    for i, v in enumerate(template):
        if v == 'X':
            template[i] = parts[i]
    return template


def ip_template(template: str, net: str) -> str:
    """
    Derive IP from a template string and a network. Both must represent IPv4 Addresses,
    and either may contain

    :param template: IP Address Template. If an octet simply contains the character 'X',
                     the X will be replaced by the corresponding octet in the network string.
                     May optionally contain a CIDR notation (which will be ignored).
    :param net: IP to replace 'X' characters with.
                May optionally contain a CIDR notation (which will be ignored).

    :returns: IP Address as a string, without CIDR
    """
    # split to cidr parts if specified
    net_parts = net.split('/', 1)
    # split to cidr parts if specified
    template_parts = template.split('/', 1)

    net_ip_parts = net_parts[0].split('.')
    template_ip_parts = template_parts[0].split('.')
    union_parts = _replace_x(template_ip_parts, net_ip_parts)
    return '.'.join(union_parts)


EventVariables.update_forward_refs()
