import argparse
import sys
from typing import Optional, List

from pydantic import BaseModel, Field

from cyboard_check import FinalResult, InputVariables, get_arg_parser


class C2GamesCheck(BaseModel, arbitrary_types_allowed=True):
    """
    Base class used to execute C2Games Check Scripts. Child classes must implement the `execute` method.
    Methods within this class should use self.print() instead of Python's builtin print()
    """
    # Parsed Arguments
    args: argparse.Namespace
    # Argument Parser
    parser: argparse.ArgumentParser
    # Input Variables
    variables: InputVariables
    # Custom Check Configuration
    config: BaseModel = Field(default_factory=BaseModel)
    # Target Host
    host: str = ""
    # Team's competition ID
    competition_id: Optional[int]
    # Final Result Class
    result: FinalResult = Field(default_factory=FinalResult)

    def __init__(self, parser=None, args: Optional[List[str]] = None, auto_exit=True, **kwargs):
        # Setup Argument Parser
        parser = get_arg_parser(parser=parser)

        # Try to parse args, passing in option argument list (for testing purposes)
        try:
            parsed = parser.parse_args(args=args)
        except SystemExit as exc:
            # Catch this because we don't necessarily want to sys.exit on an argument parsing error
            if auto_exit:
                raise
            raise RuntimeError('Argument Parsing Failed!') from exc

        # Read in Variables File based on arguments
        parsed_vars = InputVariables.variables_from_file(parsed.vars, check_name=parsed.check_name)
        vars_competition_id = parsed_vars.team_metadata.competition_id if parsed_vars.team_metadata else None

        # Setup Pydantic Class
        # pylint: disable=unexpected-keyword-arg
        super().__init__(
            parser=parser, args=parsed, variables=parsed_vars, host=parsed.host or parsed_vars.target_ip_from_vars(),
            competition_id=parsed.competition_id or vars_competition_id, **kwargs
        )

    def print(self, *args, **kwargs):
        """Only print if verbose flag is present, and print to stderr."""
        if self.args.verbose:
            print(*args, **kwargs, file=sys.stderr)

    def execute(self):
        """Execute method for subclass"""
        raise NotImplementedError()
