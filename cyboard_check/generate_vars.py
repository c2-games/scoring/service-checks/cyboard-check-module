import argparse
import json
import sys
from pathlib import Path
from typing import Dict

import yaml


def main(args: argparse.Namespace):
    """
    Read in multiple Javascript or YAML files, and output a combined JSON object.
    """
    if args.check:
        args.inputs.append(f'check:{args.check}')
    if args.team:
        args.inputs.append(f'team:{args.team}')
    if args.event:
        args.inputs.append(f'event:{args.event}')

    checks: Dict[str, Dict] = {}
    for file in args.inputs:
        try:
            check_type, path = file.split(':')
        except ValueError:
            print(f"FATAL: failed to parse variable type/path: '{file}'", file=sys.stderr)
            return

        path = Path(path)
        if not path.exists():
            print(f"FATAL: file not found: ({check_type}) '{path.resolve()}'", file=sys.stderr)
            return
        if not path.is_file():
            print(f"FATAL: file could not be loaded: ({check_type}) '{path.resolve()}'", file=sys.stderr)
            return

        if check_type in checks:
            print(
                f"WARNING: Check type '{check_type}' specified more than once! Variables will be merged.",
                file=sys.stderr
            )
        with open(path) as f:
            checks[check_type] = {
                **yaml.safe_load(f),
                **checks.get(check_type, {})
            }
    print(json.dumps(checks))
