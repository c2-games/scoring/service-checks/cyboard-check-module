from setuptools import setup


requirements = [
    "pydantic>=1.0.0,<2.0.0",
    "jinja2",
    "pyyaml"
]

setup(
    name='cyboard-check',
    setuptools_git_versioning={
        "enabled": True,
        "dev_template": "{tag}.dev{ccount}+git.{sha}",
        "dirty_template": "{tag}.dev{ccount}+git.{sha}.dirty",
    },
    description='Helper library for c2games check scripts',
    long_description=open("README.md", 'r').read(),
    author='C2 Games',
    author_email='support@c2games.org',
    url='https://gitlab.com:c2-games/scoring/service-checks/cyboard-check-module',
    download_url='https://gitlab.com/c2-games/scoring/service-checks/cyboard-check-module/-/packages',
    packages=['cyboard_check'],
    install_requires=requirements,
    setup_requires=["setuptools-git-versioning<2"],
    keywords=['c2games'],
    classifiers=[],
    package_data={'cyboard-check': ['py.typed']},
)
