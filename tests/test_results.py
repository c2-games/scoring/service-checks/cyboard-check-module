import json
import unittest
from typing import Union
from unittest.mock import patch, MagicMock

from cyboard_check import Feedback, FinalResult, ResultCode, ResultJSONEncoder


class TestFinalResult(unittest.TestCase):
    exit_args = {
        'feedback': 'Participant Feedback',
        'staff_feedback': 'Staff Feedback',
        'details': ['participant', 'details'],
        'staff_details': ['staff', 'details'],
    }
    @patch('cyboard_check.results.FinalResult.exit')
    def test_exit_success(self, mock_exit):
        FinalResult().success(**self.exit_args)
        mock_exit.assert_called_with(**self.exit_args, status=ResultCode.PASS)

    @patch('cyboard_check.results.FinalResult.exit')
    def test_exit_warn(self, mock_exit):
        FinalResult().warn(**self.exit_args)
        mock_exit.assert_called_with(**self.exit_args, status=ResultCode.WARN)

    @patch('cyboard_check.results.FinalResult.exit')
    def test_exit_fail(self, mock_exit):
        FinalResult().fail(**self.exit_args)
        mock_exit.assert_called_with(**self.exit_args, status=ResultCode.FAIL)

    @patch('cyboard_check.results.FinalResult.exit')
    def test_exit_unknown(self, mock_exit):
        FinalResult().unknown(**self.exit_args)
        mock_exit.assert_called_with(**self.exit_args, status=ResultCode.UNKNOWN)

    @patch('cyboard_check.results.FinalResult.exit')
    def test_exit_timeout(self, mock_exit):
        FinalResult().timeout(**self.exit_args)
        mock_exit.assert_called_with(**self.exit_args, status=ResultCode.TIMEOUT)

    @patch('cyboard_check.results.FinalResult.exit')
    def test_exit_error(self, mock_exit):
        FinalResult().error(**self.exit_args)
        mock_exit.assert_called_with(**self.exit_args, status=ResultCode.ERROR)


    @patch('sys.exit')
    @patch('builtins.print')
    def test_exit_with_args(self, mock_print: MagicMock, mock_exit: MagicMock):
        FinalResult().exit(ResultCode.PASS, **self.exit_args)
        mock_print.assert_called_with(json.dumps({
            'result': ResultCode.PASS.value,
            'extendedData': {
                'participant': {
                    'feedback': self.exit_args['feedback'],
                    'detailed_results': self.exit_args['details']
                },
                'staff': {
                    'feedback': self.exit_args['staff_feedback'],
                    'detailed_results': self.exit_args['staff_details']
                },
            }
        }))
        mock_exit.assert_called_with(0)

    @patch('sys.exit')
    @patch('builtins.print')
    def test_exit_without(self, mock_print: MagicMock, mock_exit: MagicMock):
        FinalResult().exit(ResultCode.PASS)
        mock_print.assert_called_with(json.dumps({
            'result': ResultCode.PASS.value,
            'extendedData': {
                'participant': {
                    'feedback': "",
                    'detailed_results': []
                },
                'staff': {
                    'feedback': "",
                    'detailed_results': []
                },
            }
        }))
        mock_exit.assert_called_with(0)

    def test_result_get_set_with_enum(self):
        r = FinalResult()
        r.result = ResultCode.PASS
        assert r.result is ResultCode.PASS

    def test_result_get_set_with_string(self):
        r = FinalResult()
        with self.assertRaises(ValueError):
            r.result = 'success'

    def test_result_get_set_with_none(self):
        r = FinalResult()
        with self.assertRaises(ValueError):
            r.result = None

    def test_participant_feedback_get_set_str(self):
        r = FinalResult()
        r.feedback = 'Hello, World!'
        assert r.feedback == 'Hello, World!'
        assert r._participant_result.feedback == 'Hello, World!'

    def test_staff_feedback_get_set_str(self):
        r = FinalResult()
        r.staff_feedback = 'Hello, World!'
        assert r.staff_feedback == 'Hello, World!'
        assert r._staff_result.feedback == 'Hello, World!'

    def test_feedback_set_none(self):
        r = FinalResult()
        with self.assertRaises(ValueError):
            r.feedback = None
        with self.assertRaises(ValueError):
            r.staff_feedback = None

    def test_result_to_json(self):
        self.maxDiff = None
        r = FinalResult()
        r.feedback = 'Hello, World!'
        r.add_detail('world.hello() was called')
        r.staff_feedback = 'The user has said hello to the world!'
        r.add_staff_detail('world.hello() was called with no arguments')
        r.result = ResultCode.PASS
        self.assertEqual(r.json(), json.dumps({
            'result': ResultCode.PASS.value,
            'extendedData': {
                'participant': {
                    'feedback': 'Hello, World!',
                    'detailed_results': ['world.hello() was called']
                },
                'staff': {
                    'feedback': 'The user has said hello to the world!',
                    'detailed_results': ['world.hello() was called with no arguments']
                },
            }
        }))


class TestFeedback(unittest.TestCase):
    def _test_feedback_details_with_val(self, val: Union[str, int, dict, list], f: Feedback = None) -> Feedback:
        f = f or Feedback()
        self.assertIsInstance(f.details, list)
        self.assertEqual(len(f.details), 0)

        f.add_details(val)

        self.assertIsInstance(f.details, list)
        self.assertEqual(len(f.details), len(val) if isinstance(val, list) else 1)

        # assert f.details == val if isinstance(val, list) else [val]
        self.assertCountEqual(f.details, val if isinstance(val, list) else [val])
        return f

    def test_feedback_details_with_string(self):
        self._test_feedback_details_with_val('hello')

    def test_feedback_details_with_int(self):
        self._test_feedback_details_with_val(1)

    def test_feedback_details_with_dict(self):
        self._test_feedback_details_with_val({'key': 'value'})

    def test_feedback_details_with_str_list(self):
        self._test_feedback_details_with_val([])

    def test_feedback_details_with_int_list(self):
        self._test_feedback_details_with_val([1, 2])

    def test_feedback_details_with_dict_list(self):
        self._test_feedback_details_with_val([{'key': 'value'}, {'key2': 'value2'}])

    def test_feedback_details_with_mixed_list(self):
        self._test_feedback_details_with_val([1, {'key2': 'value2'}, 'hello'])

    def test_feedback_details_with_multiple_additions(self):
        f = Feedback()
        f.add_details('hello')
        f.add_details([1, 2])
        f.add_details({'key': 'value'})
        f.add_details('stuff')
        self.assertCountEqual(f.details, [
            'hello',
            1, 2,
            {'key': 'value'},
            'stuff',
        ])

    def test_feedback_to_dict(self):
        f = Feedback()
        f.add_details('world.hello() was called')
        f.feedback = 'Hello, World!'
        self.assertDictEqual(f.__dict__, {
            'feedback': 'Hello, World!',
            'detailed_results': ['world.hello() was called']
        })


