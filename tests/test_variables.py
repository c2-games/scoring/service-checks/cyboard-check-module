import unittest

from cyboard_check import ip_template


class TestVariables(unittest.TestCase):
    def test_ip_template_with_all_x_with_subnets(self):
        assert ip_template("X.X.X.X/24", "172.18.13.0/16") == "172.18.13.0"

    def test_ip_template_with_all_x_no_subnet(self):
        assert ip_template("X.X.X.X", "172.18.13.0") == "172.18.13.0"

    def test_ip_template_with_one_subnet(self):
        assert ip_template("X.X.X.X/16", "172.18.13.0") == "172.18.13.0"

    def test_ip_template_with_the_other_subnet(self):
        assert ip_template("X.X.X.X", "172.18.13.0/24") == "172.18.13.0"

    def test_ip_template_with_some_x(self):
        assert ip_template("X.X.5.12", "172.18.13.0") == "172.18.5.12"

    def test_ip_template_with_some_x_v2(self):
        assert ip_template("192.168.X.X", "172.18.13.0") == "192.168.13.0"
