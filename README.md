# Cyboard Check Module

## Installation

Install this package from the gitlab pip

```sh
python3 -m pip install cyboard-check --index-url https://gitlab.com/api/v4/projects/41804200/packages/pypi/simple
```

## Development

### Dev Installation

To install this pip package from a local source, you can `pip install` the directory. The `-e` flag will install it in
"edit" mode, so changes to this check module are automatically available in the module you're developing:

```bash
pip install -e ../cyboard-check-module
```

### Usage

This module exports several helpers utilities, and can also be run directly with `python3 -m cyboard_check --help`.

### Generate Check Vars

To generate a variables file for a check script, use the module's `vars` command. Example:

```shell
python3 -m cyboard_check vars -e scoring.spec.yml -t team1.json -c checks/ssh.yml > dist/ssh.json
```

#### C2GamesCheck Class

The recommended way to integrate with this module is to inherit from the class `C2GamesCheck`.
This class automatically sets up a few things:

1. Creates an argument parser for common options and stores it as `self.parser`
2. Parses common arguments, and stores the resulting argparse.Namespace on `self.args`
3. Loads input variables file and stores in an `InputVariables` class, which is stored on `self.variables`
4. Attempts to parse the target host, and stores the result on `self.host`
5. Creates a `FinalResult` instance and stores it on `self.result`
6. Creates an Optional `self.config` which can be used to store check-specific check information in a Pydnatic Model

#### Example Usage

This is sort of the "quick start" guide - see the sections below for more detailed explanations.

```py
class C2DnsConfig(BaseModel):
  port: str
  direction: str


class DnsCheck(C2GamesCheck):
    # C2GamesCheck is a Pydantic model, so any attributes defined will be validated using Pydantic.
    resolver: dns.resolver.Resolver
    # This also means we can use Pydantic things, like `Field` to define a default value of a complex object
    resolver: dns.resolver.Resolver = Field(default_factory=dns.resolver.Resolver)

    def __init__(self):
        # Create a custom parser to add additional arguments. The standard args will still be parsed.
        # NOTE: Arguments defined here can't conflict with the arguments defined in C2GamesCheck!
        # NOTE: Don't set anything on `self` until calling `super().__init__` or you'll get an exception
        parser = argparse.ArgumentParser()

        fwd_rev_group = parser.add_mutually_exclusive_group(required=True)
        fwd_rev_group.add_argument(
            '-r', '--reverse', dest="lookup", action='store_const', const="reverse", help='do reverse lookups'
        )
        fwd_rev_group.add_argument(
            '-f', '--forward', dest="lookup", action='store_const', const="forward", help='do forward lookups'
        )
        
        # We can pass extra args to init here if we want, but there usually is no need outside of parser
        super().__init__(parser=parser)
        # After we've called Pydnatic's init function, we can finish any additional setup on self 
        self.resolver.nameservers = [self.host] # self.host is parsed from the variables file
        
        # We can optionally set self.config to something
        # The C2GamesCheck class defines its existence but sets it to None by default
        self.config = C2DnsConfig(port=53, direction=self.args.lookup)
    
    def execute(self):
        # all sub-classes should implement an execute function that takes no parameters. This is where the check logic goes.
        # NOTE: ALL Print Statements should use self.print, not the builtin print. self.print statements will
        #       only print when -v/--verbose is passed to the script
        self.print(f'performing a {self.config.direction} lookup against {self.host}:{self.config.port}')
        domains = {}
        # Variables read from the input file can be accessed using self.variables. There is no typing on these fields
        for k, v in self.variables.check.domains.items():
            # Use variables.jinja_render_vars to render any jinja templating that occurs in the variables
            k = self.variables.jinja_render_vars(k)
            v = self.variables.jinja_render_vars(v)
            domains[k] = v
            
        for domain, ip in domains.items():
            try:
                # [do fancy checks with domain/ip]
                self.result.success()
            except Exception as e:
                # Scripts should call self.result.exit (or one of it's helpers) to finish execution and print the result
                self.result.partial(staff_details={'raw': e})
        
# run the class and it's execute function if this file is called directly
if __name__ == "__main__":
    DnsCheck().execute()
```

#### Requirements

Classes Implementing C2GamesCheck must:

- Implement an `execute` method
- Call `self.result.exit()` either directly or using one of the helpers (`result.success()`, `result.warn()`, etc)
- Pass the following arguments when used:
  - `--vars <valid json file>`

#### Extending jinja templating

To add custom Jinja global functions, you can pass a dictionary of functions as `jinja_globals` to `InputVariables`
class.

There are a few built-in functions that can be used:

- uuid() - Generates a UUIDv4
- random_string(length, chars) - Generates a random string of the specified length
  - length: int - Length of the string to generate. Defaults to `10`
  - chars: str - Characters to use in the string. Defaults to `string.ascii_letters + string.digits`
- now(utc, fmt): Get the current datetime
  - utc: bool - Return the current UTC time. Defaults to `True`
  - fmt: str - Format string to return the datetime in. Defaults to `None`

To keep the built-in functions when specifying custom functions, import the function `default_jinja_globals` and
use it with your new values:

```py
InputVariables(
  jinja_globals={
    'custom_thing': custom_function,
    **default_jinja_globals()
  }
)
```

#### Extending Parser

By default, the C2GamesCheck class parses several parameters:

```sh
usage: __main__.py [-h] --vars vars.json [-v] [-C CHECK_NAME] [-t TIMEOUT]

options:
  -h, --help            show this help message and exit
  --vars vars.json      variables file
  -v, --verbose         print debug information
  -C CHECK_NAME, --check-name CHECK_NAME
                        name of check being executed
  -t TIMEOUT, --timeout TIMEOUT
                        Timeout for command (not supported by all checks)
```

To provide additional arguments, create an ArgumentParser object and pass it to the init function of C2GamesCheck.
Example:

```py
class DnsCheck(C2GamesCheck):
    def __init__(self):
        parser = argparse.ArgumentParser()

        fwd_rev_group = parser.add_mutually_exclusive_group(required=True)
        fwd_rev_group.add_argument(
            '-r', '--reverse', dest="lookup", action='store_const', const="reverse", help='do reverse lookups'
        )
        fwd_rev_group.add_argument(
            '-f', '--forward', dest="lookup", action='store_const', const="forward", help='do forward lookups'
        )

        super().__init__(parser=parser)
```

#### Exceptions Raised

Multiple exceptions can be raised during the initialization of the C2Games Check class.

##### Target IP Not Found

If the target IP can't be found from a deployment config or the event.checks array, a `RuntimeError` will be raised
stating that "Target IP was not found."

This is most commonly caused by not passing the check name using `-C`/`--check-name`

```py
# Bad Example
>>> C2GamesCheck(auto_exit=False, args=['--vars', '../support/dist/www_content.json'])
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "/home/bdavis/Dev/cyboard-check-module/cyboard_check/c2games_check.py", line 42, in __init__
    host = parsed_vars.target_ip_from_vars()
  File "/home/bdavis/Dev/cyboard-check-module/cyboard_check/variables.py", line 90, in target_ip_from_vars
    raise RuntimeError('Target IP was not found')
RuntimeError: Target IP was not found

# Good Example
>>> C2GamesCheck(auto_exit=False, args=['--vars', '../support/dist/www_content.json', '-C', 'external_www_content'])
C2GamesCheck(...)
```

##### Error Parsing Arguments

If an error occurs when parsing CLI arguments, such as missing a value for `--vars`, argparse will raise a `SystemExit`
Exception. If not handled, this exception doesn't simply bubble up, it will cause Python to exit with exit status 2.

If the desired behavior is to raise an exception and not exit, `auto_exit=False` can be passed to the initialization
function. This will catch the `SystemExit` error, and instead through a `RuntimeError, which can be useful for testing purposes.

```py
# Using C2GamesCheck Directly (ex, unit tests)
C2GamesCheck(auto_exit=False, args=['--vars', 'asdf'])

# Specify as part of a subclass
class CustomCheck(C2GamesCheck):
    def __init__(self):
        super().__init__(auto_error=False)
```

##### Error Reading Variables File

Multiple File I/O related errors can be raised from attempting to read the contents of the file specified with `--vars`.
Any number of File errors could be raised - Ex, `FileNotFoundError` or `PermissionError`.

A `json.decoder.JSONDecodeError` can be thrown if the JSON file being loaded is invalid.

##### Parser Conflict Error

If passing in a custom Parser, a `argparse.ArgumentError` can be raised if there is a conflict in arguments. Example:

```sh
argparse.ArgumentError: argument -C/--check-name: conflicting option string: -C
```
